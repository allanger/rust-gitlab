// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.

//! Group shared projects API endpoints.
//!
//! These endpoints are used for querying group projects.

pub mod add;
pub mod remove;
pub mod shared;

pub use self::add::AddGroupShare;
pub use self::add::AddGroupShareBuilder;
pub use self::add::AddGroupShareBuilderError;

pub use self::remove::RemoveGroupShare;
pub use self::remove::RemoveGroupShareBuilder;
pub use self::remove::RemoveGroupShareBuilderError;

pub use self::shared::GroupSharedProjects;
pub use self::shared::GroupSharedProjectsBuilder;
pub use self::shared::GroupSharedProjectsBuilderError;
pub use self::shared::GroupSharedProjectsOrderBy;
