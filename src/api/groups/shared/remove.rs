// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.

use derive_builder::Builder;

use crate::api::common::NameOrId;
use crate::api::endpoint_prelude::*;

/// Add a user as a member of a Group.
#[derive(Debug, Builder)]
#[builder(setter(strip_option))]
pub struct RemoveGroupShare<'a> {
    /// The ID or URL-encoded path of the group
    #[builder(setter(into))]
    id: NameOrId<'a>,
    /// The ID of the group to share with
    #[builder(setter(name = "_group"), private)]
    group_id: u64,
}

impl<'a> RemoveGroupShare<'a> {
    /// Create a builder for the endpoint.
    pub fn builder() -> RemoveGroupShareBuilder<'a> {
        RemoveGroupShareBuilder {
            ..Default::default()
        }
    }

    /// Create an ancestor-including builder for the endpoint.
    pub fn all_builder() -> RemoveGroupShareBuilder<'a> {
        RemoveGroupShareBuilder {
            ..Default::default()
        }
    }
}

impl<'a> RemoveGroupShareBuilder<'a> {
    /// The group to remove (by ID).
    pub fn group(&mut self, group_id: u64) -> &mut Self {
        self.group_id = Some(group_id);
        self
    }
}

impl<'a> Endpoint for RemoveGroupShare<'a> {
    fn method(&self) -> Method {
        Method::DELETE
    }

    fn endpoint(&self) -> Cow<'static, str> {
        format!("groups/{}/share/{}", self.id, self.group_id).into()
    }

    fn body(&self) -> Result<Option<(&'static str, Vec<u8>)>, BodyError> {
        let params = FormParams::default();
        params.into_body()
    }
}

#[cfg(test)]
mod tests {
    use http::Method;

    use crate::api::groups::shared::{RemoveGroupShare, RemoveGroupShareBuilderError};
    use crate::api::{self, Query};
    use crate::test::client::{ExpectedUrl, SingleTestClient};

    #[test]
    fn all_parameters_are_needed() {
        let err = RemoveGroupShare::builder().build().unwrap_err();
        crate::test::assert_missing_field!(err, RemoveGroupShareBuilderError, "id");
    }

    #[test]
    fn group_id_is_necessary() {
        let err = RemoveGroupShare::builder().id(1).build().unwrap_err();
        crate::test::assert_missing_field!(err, RemoveGroupShareBuilderError, "group_id");
    }

    #[test]
    fn sufficient_parameters() {
        RemoveGroupShare::builder().id(1).group(1).build().unwrap();
    }

    #[test]
    fn endpoint() {
        let endpoint = ExpectedUrl::builder()
            .method(Method::DELETE)
            .endpoint("groups/group%2Fsubgroup/share/1")
            .content_type("application/x-www-form-urlencoded")
            .build()
            .unwrap();
        let client = SingleTestClient::new_raw(endpoint, "");

        let endpoint = RemoveGroupShare::builder()
            .id("group/subgroup")
            .group(1)
            .build()
            .unwrap();
        api::ignore(endpoint).query(&client).unwrap();
    }
}
