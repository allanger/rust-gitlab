// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.

use chrono::NaiveDate;
use derive_builder::Builder;

use crate::api::common::{AccessLevel, NameOrId};
use crate::api::endpoint_prelude::*;

/// Share group with another group.
#[derive(Debug, Builder)]
#[builder(setter(strip_option))]
pub struct AddGroupShare<'a> {
    /// The ID or URL-encoded path of the group.
    #[builder(setter(into))]
    id: NameOrId<'a>,
    /// The ID of the group to share with.
    #[builder(setter(into), private)]
    group_id: u64,
    /// The access level to grant the group.
    group_access: AccessLevel,
    /// When the group's access expires.
    #[builder(default)]
    expires_at: Option<NaiveDate>,
}

impl<'a> AddGroupShare<'a> {
    /// Create a builder for the endpoint.
    pub fn builder() -> AddGroupShareBuilder<'a> {
        AddGroupShareBuilder {
            ..Default::default()
        }
    }

    /// Create an ancester-including builder for the endpoint.
    pub fn all_builder() -> AddGroupShareBuilder<'a> {
        AddGroupShareBuilder {
            ..Default::default()
        }
    }
}

impl<'a> AddGroupShareBuilder<'a> {
    /// The group to add (by ID).
    pub fn group(&mut self, group_id: u64) -> &mut Self {
        self.group_id = Some(group_id);
        self
    }
}

impl<'a> Endpoint for AddGroupShare<'a> {
    fn method(&self) -> Method {
        Method::POST
    }

    fn endpoint(&self) -> Cow<'static, str> {
        format!("groups/{}/share", self.id).into()
    }

    fn body(&self) -> Result<Option<(&'static str, Vec<u8>)>, BodyError> {
        let mut params = FormParams::default();

        params
            .push("group_id", self.group_id)
            .push("group_access", self.group_access.as_u64())
            .push_opt("expires_at", self.expires_at);

        params.into_body()
    }
}

#[cfg(test)]
mod tests {
    use chrono::NaiveDate;
    use http::Method;

    use crate::api::common::AccessLevel;
    use crate::api::groups::shared::{AddGroupShare, AddGroupShareBuilderError};
    use crate::api::{self, Query};
    use crate::test::client::{ExpectedUrl, SingleTestClient};

    #[test]
    fn all_parameters_are_needed() {
        let err = AddGroupShare::builder().build().unwrap_err();
        crate::test::assert_missing_field!(err, AddGroupShareBuilderError, "id");
    }

    #[test]
    fn id_is_necessary() {
        let err = AddGroupShare::builder()
            .group(1)
            .group_access(AccessLevel::Developer)
            .build()
            .unwrap_err();
        crate::test::assert_missing_field!(err, AddGroupShareBuilderError, "id");
    }

    #[test]
    fn group_id_is_necessary() {
        let err = AddGroupShare::builder()
            .id(1)
            .group_access(AccessLevel::Developer)
            .build()
            .unwrap_err();
        crate::test::assert_missing_field!(err, AddGroupShareBuilderError, "group_id");
    }

    #[test]
    fn group_access_is_necessary() {
        let err = AddGroupShare::builder()
            .id(1)
            .group(1)
            .build()
            .unwrap_err();
        crate::test::assert_missing_field!(err, AddGroupShareBuilderError, "group_access");
    }

    #[test]
    fn sufficient_parameters() {
        AddGroupShare::builder()
            .id("id")
            .group(1)
            .group_access(AccessLevel::Developer)
            .build()
            .unwrap();
    }

    #[test]
    fn endpoint() {
        let endpoint = ExpectedUrl::builder()
            .method(Method::POST)
            .endpoint("groups/simple%2Fproject/share")
            .content_type("application/x-www-form-urlencoded")
            .body_str(concat!("group_id=1", "&group_access=30"))
            .build()
            .unwrap();
        let client = SingleTestClient::new_raw(endpoint, "");

        let endpoint = AddGroupShare::builder()
            .id("simple/project")
            .group(1)
            .group_access(AccessLevel::Developer)
            .build()
            .unwrap();
        api::ignore(endpoint).query(&client).unwrap();
    }

    #[test]
    fn endpoint_expires_at() {
        let endpoint = ExpectedUrl::builder()
            .method(Method::POST)
            .endpoint("groups/1/share")
            .content_type("application/x-www-form-urlencoded")
            .body_str(concat!(
                "group_id=1",
                "&group_access=30",
                "&expires_at=2020-01-01",
            ))
            .build()
            .unwrap();
        let client = SingleTestClient::new_raw(endpoint, "");

        let endpoint = AddGroupShare::builder()
            .id(1)
            .group(1)
            .group_access(AccessLevel::Developer)
            .expires_at(NaiveDate::from_ymd(2020, 1, 1))
            .build()
            .unwrap();
        api::ignore(endpoint).query(&client).unwrap();
    }
}
