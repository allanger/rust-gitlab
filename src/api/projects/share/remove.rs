// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.

use derive_builder::Builder;

use crate::api::common::NameOrId;
use crate::api::endpoint_prelude::*;

/// Add a user as a member of a project.
#[derive(Debug, Builder)]
#[builder(setter(strip_option))]
pub struct RemoveProjectShare<'a> {
    /// The project to add the user to.
    #[builder(setter(into))]
    project: NameOrId<'a>,
    /// The user to add to the project.
    #[builder(setter(name = "_group"), private)]
    group: u64,
}

impl<'a> RemoveProjectShare<'a> {
    /// Create a builder for the endpoint.
    pub fn builder() -> RemoveProjectShareBuilder<'a> {
        RemoveProjectShareBuilder {
            ..Default::default()
        }
    }

    /// Create an ancester-including builder for the endpoint.
    pub fn all_builder() -> RemoveProjectShareBuilder<'a> {
        RemoveProjectShareBuilder {
            ..Default::default()
        }
    }
}

impl<'a> RemoveProjectShareBuilder<'a> {
    /// The user to add (by ID).
    pub fn group(&mut self, group: u64) -> &mut Self {
        self.group = Some(group);
        self
    }
}

impl<'a> Endpoint for RemoveProjectShare<'a> {
    fn method(&self) -> Method {
        Method::DELETE
    }

    fn endpoint(&self) -> Cow<'static, str> {
        format!("projects/{}/share/{}", self.project, self.group).into()
    }

    fn body(&self) -> Result<Option<(&'static str, Vec<u8>)>, BodyError> {
        let params = FormParams::default();
        params.into_body()
    }
}

#[cfg(test)]
mod tests {
    use http::Method;

    use crate::api::projects::share::{RemoveProjectShare, RemoveProjectShareBuilderError};
    use crate::api::{self, Query};
    use crate::test::client::{ExpectedUrl, SingleTestClient};

    #[test]
    fn all_parameters_are_needed() {
        let err = RemoveProjectShare::builder().build().unwrap_err();
        crate::test::assert_missing_field!(err, RemoveProjectShareBuilderError, "project");
    }

    #[test]
    fn project_is_necessary() {
        let err = RemoveProjectShare::builder().build().unwrap_err();
        crate::test::assert_missing_field!(err, RemoveProjectShareBuilderError, "project");
    }

    #[test]
    fn sufficient_parameters() {
        let err = RemoveProjectShare::builder()
            .project("project")
            .build()
            .unwrap_err();
        crate::test::assert_missing_field!(err, RemoveProjectShareBuilderError, "group");
    }

    #[test]
    fn endpoint() {
        let endpoint = ExpectedUrl::builder()
            .method(Method::DELETE)
            .endpoint("projects/project%2Fsubproject/share/1")
            .content_type("application/x-www-form-urlencoded")
            .build()
            .unwrap();
        let client = SingleTestClient::new_raw(endpoint, "");

        let endpoint = RemoveProjectShare::builder()
            .project("project/subproject")
            .group(1)
            .build()
            .unwrap();
        api::ignore(endpoint).query(&client).unwrap();
    }
}
